extends Control


signal new_game_pressed


func _ready() -> void:
	$Logo/CPUParticles2D.emitting = true
	$AnimationPlayer.play("logo_idle")


func _gui_input(event: InputEvent) -> void:
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.pressed:
		if $BtnNewGame.pressed:
			emit_signal("new_game_pressed")


func show_hide_menu(show: bool) -> void:
	if show:
		$Logo/CPUParticles2D.emitting = true
		$AnimationPlayer.play("logo_idle")
		self.show()
	else:
		$Logo/CPUParticles2D.emitting = false
		$AnimationPlayer.stop()
		self.hide()