extends Node


func _ready() -> void:
	var _new_game_pressed = $Menu.connect("new_game_pressed", self, "_on_Menu_new_game_pressed")
	var _back_pressed = $Game.connect("back_pressed", self, "_on_Game_back_pressed")


func _on_Menu_new_game_pressed() -> void:
	$Menu.show_hide_menu(false)
	$Game.show_hide_game(true)


func _on_Game_back_pressed() -> void:
	$Menu.show_hide_menu(true)
	$Game.show_hide_game(false)
