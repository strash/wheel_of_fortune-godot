extends Control


signal back_pressed


var score:float = 0.0
var is_score_set = false

export var sections_count:float = 12
var section_size:float = 360 / sections_count
var section_price:float = 100.0

var speed = 0
const START_SPEED:float = 1500.0
const SPEED_DRAG:float = 5.0
onready var wheel:TextureRect = get_node("Wheel")


func _ready() -> void:
	$Score/Label.text = "$ %s" % score

func _process(delta: float) -> void:
	if speed != 0:
		wheel.rect_rotation += delta * speed
		if wheel.rect_rotation > 360:
			wheel.rect_rotation = fmod(wheel.rect_rotation, 360)
		speed = speed - SPEED_DRAG
		if speed < 0:
			speed = 0
	else:
		if fmod(wheel.rect_rotation, section_size) != 0:
			var align_rotation: float
			if fmod(wheel.rect_rotation, section_size) > section_size / 2:
				align_rotation = round(wheel.rect_rotation + ( section_size - fmod(wheel.rect_rotation, section_size) ))
			else:
				align_rotation = round( wheel.rect_rotation - fmod(wheel.rect_rotation, section_size) )
			if not is_score_set:
				$Tween.interpolate_property(wheel, "rect_rotation", wheel.rect_rotation, align_rotation, 0.1)
				var new_score = score + ((section_price * sections_count) - (fmod(align_rotation, 360) / section_size * section_price - section_price))
				_set_score(new_score)
	$Score/Label.text = "$ %s" % round(score)


func _set_score(new_score: int) -> void:
	$Tween.interpolate_property(self, "score", score, new_score, 0.7)
	if not $Tween.is_active():
		$Tween.start()
	is_score_set = true


func _gui_input(event: InputEvent) -> void:
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.pressed:
		if $BtnBack.pressed:
			emit_signal("back_pressed")
		if $BtnSpin.pressed and speed == 0:
			speed = START_SPEED
			is_score_set = false


func show_hide_game(show: bool) -> void:
	if show:
		self.show()
	else:
		self.hide()
